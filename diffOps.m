function [ D1, D4 ] = diffOps(order_diff,no_x,dx);
switch order_diff
    case 2 % interior order 2, boundary order 2
            % 1st derivative operator
            diag1       = ones(no_x-1,1)*0.5;
            D1          = diag(zeros(no_x,1)) + diag(diag1,1) + diag(-diag1,-1);
            D1(1,:)     = [-1,1,zeros(1,no_x-2)];
            D1(end,:)   = [zeros(1,no_x-2),-1,1];
            D1          = D1/dx;

            % 4th derivative operator
            diag0       = ones(no_x,1)*6;
            diag1       = ones(no_x-1,1)*(-4);
            diag2       = ones(no_x-2,1);
            D4          = diag(diag0,0) + diag(diag1,1) + diag(diag1,-1) + diag(diag2,2) + diag(diag2,-2);
            D4(1,:)     = [3,-14,26,-24,11,-2,zeros(1,no_x-6)];
            D4(2,:)     = [0,3,-14,26,-24,11,-2,zeros(1,no_x-7)];
            D4(end-1,:) = [zeros(1,no_x-7),-2,11,-24,26,-14,3,0];
            D4(end,:)   = [zeros(1,no_x-6),-2,11,-24,26,-14,3];
            D4          = D4/dx^4;
    case 4 % interior order 4, boundary order 2
            % 1st derivative operator
            diag1       = ones(no_x-1,1)*2/3;
            diag2       = ones(no_x-2,1)*1/12;
            D1          = diag(zeros(no_x,1)) + diag(diag1,1) + diag(-diag1,-1) + diag(-diag2,2) + diag(diag2,-2);
            D1(1,:)     = [-3/2,2,-1/2,zeros(1,no_x-3)];
            D1(2,:)     = [0,-3/2,2,-1/2,zeros(1,no_x-4)];
            D1(end-1,:) = [zeros(1,no_x-4),-3/2,2,-1/2,0];
            D1(end,:)   = [zeros(1,no_x-3),-3/2,2,-1/2];
            D1          = D1/dx;

            % 4th derivative operator
            diag0       = ones(no_x,1)*28/3;
            diag1       = ones(no_x-1,1)*(-13/2);
            diag2       = ones(no_x-2,1)*2;
            diag3       = ones(no_x-3,1)*(-1/6);
            D4          = diag(diag0,0) + diag(diag1,1) + diag(diag1,-1) + diag(diag2,2) + diag(diag2,-2) + diag(diag3,3) + diag(diag3,-3);
            D4(1,:)     = [3,-14,26,-24,11,-2,zeros(1,no_x-6)];
            D4(2,:)     = [0,3,-14,26,-24,11,-2,zeros(1,no_x-7)];
            D4(3,:)     = [0,0,3,-14,26,-24,11,-2,zeros(1,no_x-8)];
            D4(end-2,:) = [zeros(1,no_x-8),2,-11,24,-26,14,-3,0,0];
            D4(end-1,:) = [zeros(1,no_x-7),2,-11,24,-26,14,-3,0];
            D4(end,:)   = [zeros(1,no_x-6),2,-11,24,-26,14,-3];
            D4          = D4/dx^4;
end
end

