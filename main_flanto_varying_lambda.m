clear all; clc; close all;

save_plots  = 1;

%% numerical parameters
no_x        = 100; % number of grid points for panel discretization
order_diff  = 2; % order of differentiation scheme: 2, 4
BC_mode     = 'simplysupported'; % 'clamped' or 'simplysupported'

max_sign_changes = 10;
max_imag_val     = 201;

lambdas     = [linspace(0,330,50),linspace(330,400,40)];
no_lambdas  = length(lambdas);

%% compute auxiliary variables
x           = linspace(0,1,no_x);
dx          = x(2)-x(1);

%% set up differentiation operators
[D1,D4]     = diffOps(order_diff,no_x,dx);
%D4          = D1^4;

for ii=1:no_lambdas
    clc
    disp([num2str(ii/no_lambdas*100,'%0.0f'),'%'])
    
    %% set up EVP
    A   = D4+lambdas(ii)*D1;
    B   = eye(no_x);

    %% set up BCs (clamped-clamped)
    switch BC_mode
        case 'clamped' % clamped-clamped: W=W'=0
            A(1,:)          = zeros(1,no_x);
            A(1,1)          = 1;
            B(1,:)          = zeros(1,no_x);

            A(2,:)          = zeros(1,no_x);
            A(2,1)          = 1;
            A(2,2)          = -1;
            B(2,:)          = zeros(1,no_x);

            A(end-1,:)      = zeros(1,no_x);
            A(end-1,end-1)  = -1;
            A(end-1,end)    = 1;
            B(end-1,:)      = zeros(1,no_x);

            A(end,:)        = zeros(1,no_x);
            A(end,end)      = 1;
            B(end,:)        = zeros(1,no_x);
        case 'simplysupported' % simply supported: W=W''=0
            A(1,:)          = [1,zeros(1,no_x-1)];
            B(1,:)          = zeros(1,no_x);

            A(2,:)          = [1,-2,1,zeros(1,no_x-3)];
            B(2,:)          = zeros(1,no_x);

            A(end-1,:)      = [zeros(1,no_x-3),1,-2,1];
            B(end-1,:)      = zeros(1,no_x);

            A(end,:)        = [zeros(1,no_x-1),1];
            B(end,:)        = zeros(1,no_x);
    end     

    %% solve EVP
    [vecs,Lambdas]  = eig(A,B);
    Omegas          = sqrt(-diag(Lambdas));

    %% save eigenvalues and vectors
    Omegas_global(:,ii)     = Omegas;
    vecs_global(:,:,ii)     = vecs;
end

%% delete spectrum outside of desired range
for ii=1:no_lambdas
    [tmp,indices]               = sort(imag(Omegas_global(:,ii)),'ascend');
    [i1 j1] = find(tmp>0, 1, 'first');
    [i2 j2] = find(tmp>max_imag_val, 1, 'first');
    indices2    = indices(i1:i2);
    
    data(ii).imag  = imag(Omegas_global(indices2,ii));
    data(ii).real  = real(Omegas_global(indices2,ii));
    data(ii).vecs    = vecs_global(:,indices2,ii);
end

%% delete numerical eigenvalues (highly oscillatory due to repeated first derivative)
for ii=1:no_lambdas
    [js,ns] = size(data(ii).vecs);
    for nn=ns:-1:1
        counter = 0;
        for jj=1:js-1
            product = real(data(ii).vecs(jj,nn))*real(data(ii).vecs(jj+1,nn));
            if (product<0)
                counter = counter + 1;
            end
        end
        if(counter>max_sign_changes) % delete mode
            
            data(ii).vecs(:,nn)    = [];
            data(ii).imag(nn)    = [];
            data(ii).real(nn)    = [];
        end
    end
end

%% plot
close all;
disp('Plotting stuff...')
xlimits     = [0,lambdas(end)];

[ Mode1, Mode2, Mode3, Mode4, Im_mode ] = LitData2();

figure(1); hold off
for ii=1:no_lambdas
    plot(lambdas(ii),data(ii).imag,'r.','MarkerSize',14); hold on
end
plot(Mode1(:,1),Mode1(:,2),'ks')
plot(Mode2(:,1),Mode2(:,2),'ks')
plot(Mode3(:,1),Mode3(:,2),'ks')
plot(Mode4(:,1),Mode4(:,2),'ks')
xlim(xlimits)
ylim([0,200])
set(gca,'FontSize',16,'XTick',[0,100,200,300,400],'YTick',[0,50,100,150,200])
xlabel('$\lambda$','Interpreter','Latex','FontSize',24)
ylabel('Im($\omega$)','Interpreter','Latex','FontSize',24)

set(gcf,'Position',[200 500 400 400])
set(gcf,'PaperPositionMode','Auto')
if(save_plots)
    print(gcf,['results/eval_varying_lambda_Im_nx',num2str(no_x),'_nlambda',num2str(no_lambdas),'_',BC_mode],'-djpeg90')
    print(gcf,['results/eval_varying_lambda_Im_nx',num2str(no_x),'_nlambda',num2str(no_lambdas),'_',BC_mode],'-depsc')
    savefig(gcf,['results/eval_varying_lambda_Im_nx',num2str(no_x),'_nlambda',num2str(no_lambdas),'_',BC_mode])
end

figure(2); hold off
for ii=1:no_lambdas
    plot(lambdas(ii),data(ii).real,'r.','MarkerSize',14); hold on
end
plot(Im_mode(:,1),Im_mode(:,2),'ks')
xlim(xlimits)
ylim([0,8])
set(gca,'FontSize',16,'XTick',[0,100,200,300,400],'YTick',[0,2,4,6,8])
xlabel('$\lambda$','Interpreter','Latex','FontSize',24)
ylabel('Re($\omega$)','Interpreter','Latex','FontSize',24)

set(gcf,'Position',[600 500 400 400])
set(gcf,'PaperPositionMode','Auto')
if(save_plots)
    print(gcf,['results/eval_varying_lambda_Re_nx',num2str(no_x),'_nlambda',num2str(no_lambdas),'_',BC_mode],'-djpeg90')
    print(gcf,['results/eval_varying_lambda_Re_nx',num2str(no_x),'_nlambda',num2str(no_lambdas),'_',BC_mode],'-depsc')
    savefig(gcf,['results/eval_varying_lambda_Re_nx',num2str(no_x),'_nlambda',num2str(no_lambdas),'_',BC_mode])
end









